var webpack = require("webpack");
var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');

var APP_DIR = path.resolve(__dirname, "../src/ts");
var STYLE_DIR = path.resolve(__dirname, "../src/stylesheets");
var COMMON_DIR = path.resolve(__dirname, "../src/common");

var DIST_DIR = path.resolve(__dirname, "../dist");

module.exports = {
    entry: {
        "public/styles": STYLE_DIR + "/application.scss",
        "public/application": APP_DIR + "/main.ts"
    },
    output: {
        path: DIST_DIR,
        filename: "[name].js"
    },
    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"],
    },
    module: {
        loaders: [
            { test: /\.tsx?$/, loader: 'ts-loader' },
            { test: /\.json$/, loader: 'json-loader' },
            {
                test: /\.scss$/,
                include: STYLE_DIR,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader!sass-loader"
                })
            }]
    },
    plugins: [
        new ExtractTextPlugin("public/style.css", {
            allChunks: true
        }),
        new CopyWebpackPlugin([
            { from: COMMON_DIR, to: DIST_DIR + "/public" },
        ])
    ]
};

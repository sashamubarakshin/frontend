const fs = require("fs");
const ejs = require("ejs");

const BASE_PATH = "dist/public";
const ejsPath = BASE_PATH + "/report.ejs";

if (!module.parents) {
    const jsonPath = process.argv[2];
    if (!jsonPath) {
        throw new Error("No path passed");
    }
    const json = JSON.parse(fs.readFileSync(jsonPath).toString());
    const templateString = fs
        .readFileSync(ejsPath)
        .toString();
    const htmlString = ejs.render(templateString, json);
    fs.writeFileSync(BASE_PATH + "/report.html", htmlString);
}
